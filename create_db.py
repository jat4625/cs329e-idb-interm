# beginning of create_db.py
import json
from models import app, db, Dogs
def load_json(filename):
     with open(filename) as file:
          jsn = json.load(file)
          file.close()
     return jsn
def create_books():
     book = load_json('dogs.json')
     for oneBook in book['Dogs']:
          id = oneBook['id']
          name = oneBook['name']
          page = oneBook['page']
          shelter = oneBook['shelter']
          img = oneBook['img']
          size = oneBook['size']
          sex = oneBook['sex']
          newBook = Dogs(id=id,name=name,page=page,shelter=shelter,img= img,size=size,sex= sex)

          # After I create the book, I can then add it to my session.
          db.session.add(newBook)
          # commit the session to my DB.
          db.session.commit()
     create_books()
