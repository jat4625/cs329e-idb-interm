import json
sample = {'Dogs':\
        [{'id':1,'page':'dog_mayla', 'name':'Mayla','shelter': 'Austin Animal Center','img':"/static/images/dog_mayla.jpg",'size':'medium','sex':"Female"},
         {'id':2,'page':'dog_coco', 'name':'Coco','shelter':'Austin Humane Society','img':"/static/images/dog_coco.jpg",'size':'large','sex': "Female"},
         {'id':3,'page':'dog_whiskey', 'name':'Whiskey','shelter':'Austin Pets Alive','img':"/static/images/dog_whiskey.jpg",'size':'large', 'sex':"Male"}]}
with open('dogs.json', 'w') as fp:
 json.dump(sample, fp, indent=4)