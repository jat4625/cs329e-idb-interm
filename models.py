from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:ot.ft.2016@localhost:5432/idb')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)
class Dogs(db.Model):
    __tablename__ = 'Dogs'
    id = db.Column(db.Integer,primary_key = True)
    name = db.Column(db.String(80), nullable = False)
    page = db.Column(db.String(80), nullable = False)
    #breed = db.Column(db.String(80), nullable = False)
    size = db.Column(db.String(80), nullable = False)
    sex = db.Column(db.String(80), nullable = False)
    shelter = db.Column(db.String(80), nullable = False)
    #age = db.Column(db.String(80), nullable = False)
    img = db.Column(db.String(80), nullable = False)
    #description = db.Column(db.String(80), nullable = False)
'''
class DogBreeds(db.Model):
    __tablename__ = 'breeds'
    name = db.Column(db.String(80), nullable = False)
    breed = db.Column(db.String(80), nullable = False)
    size = db.Column(db.String(80), nullable = False)
    shelter = db.Column(db.String(80), nullable = False)
    age = db.Column(db.String(80), nullable = False)
    img = db.Column(db.String(80), nullable = False)
class Shelters(db.Model):
    __tablename__ = 'shelters'
    name = db.Column(db.String(80), nullable = False)
    breed = db.Column(db.String(80), nullable = False)
    size = db.Column(db.String(80), nullable = False)
    shelter = db.Column(db.String(80), nullable = False)
    age = db.Column(db.String(80), nullable = False)
    img = db.Column(db.String(80), nullable = False)
'''
db.create_all()