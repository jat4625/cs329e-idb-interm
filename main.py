from flask import render_template
from create_db import app, db, Dogs, create_books
#---------------------(breeds)-------------------------------
@app.route('/breeds/breed_shepherd')
def breed_shepherd():
    return render_template("breed_shepherd.html")
@app.route('/breeds/breed_retriever')
def breed_retriever():
    return render_template("breed_retriever.html")
@app.route('/breeds/breed_boxer')
def breed_boxer():
    return render_template("breed_boxer.html")
@app.route('/breeds/')
def breeds():
    return render_template("dog_breed.html")
#-----------------------(Dogs)-------------------------------
@app.route('/dogs/mayla')
def dog_mayla():
    return render_template("dog_mayla.html")
@app.route('/dogs/coco')
def dog_coco():
    return render_template("dog_coco.html")
@app.route('/dogs/whiskey')
def dog_whiskey():
    return render_template("dog_whiskey.html")

@app.route('/dogs/')
def dogs():
    dog = db.session.query(Dogs).all()
    return render_template("dogs.html", dogs = dog)
#---------------------(Shelters)-------------------------------
@app.route('/shelters/AustinHumaneSociety/')
def AHS():
    return render_template("Austin_human_society.html")
@app.route('/shelters/AustinAnimalCenter/')
def AAC():
    return render_template("Austin_Animal_center.html")
@app.route('/shelters/AustinPetsAlive/')
def APA():
    return render_template("Austin_pets_alive.html")
@app.route('/shelters/')
def shelters():
    return render_template("shelters.html")
#---------------------(Personal)-------------------------------
@app.route('/About')
def About():
    return render_template("About.html")
@app.route('/')
def index():
    return render_template("index.html")
if __name__ == "__main__":
    app.run()
